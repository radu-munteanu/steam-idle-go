package main

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/anaskhan96/soup"
)

const badgeURLFormatString = "https://steamcommunity.com/profiles/%s/badges%s"

// GetBadges constructs a list of badges using a logged in client
func GetBadges(client *Client) {
	var allBadges []Badge
	currentPage := "?p=1"

	for {
		fmt.Printf("Processing games from page %s...\n", strings.TrimPrefix(currentPage, "?p="))
		badgeURL := fmt.Sprintf(badgeURLFormatString, client.Steamid, currentPage)
		var badges []Badge
		badges, currentPage = getBadges(badgeURL, client)
		allBadges = append(allBadges, badges...)

		if currentPage == "" {
			break
		}
	}
	client.Badges = allBadges
}

func getBadges(badgeURL string, client *Client) ([]Badge, string) {
	var badges []Badge
	var nextPage string

	if resp, err := soup.GetWithClient(badgeURL, &client.HTTPClient); err == nil {
		root := soup.HTMLParse(resp)
		badgeRows := root.FindAll("div", "class", "badge_row")
		for _, badgeRow := range badgeRows {
			badge, isPlayable := NewBadge(badgeRow)
			if isPlayable && badge.Remaining > 0 {
				badges = append(badges, *badge)
			}
		}
		nextPage = getNextPage(root)
	}
	return badges, nextPage
}

// Badge represents all relevant info about a badge
type Badge struct {
	Node      soup.Root
	Appid     string
	Title     string
	Playtime  float64
	Received  int64
	Remaining int64
}

// NewBadge parses a badge DOM node to construct a Badge
func NewBadge(node soup.Root) (*Badge, bool) {
	badge := Badge{Node: node}
	if node.Find("div", "class", "card_drop_info_header").Error != nil {
		return nil, false
	}
	badge.getAppid()
	badge.getTitle()
	if strings.HasSuffix(badge.Title, "- Foil Badge") {
		return nil, false
	}
	badge.getPlaytime()
	badge.getReceived()
	badge.getRemaining()
	return &badge, true
}

func (badge Badge) String() string {
	return fmt.Sprintf("{%v %v %v %v %v}", badge.Appid, badge.Title, badge.Playtime, badge.Received, badge.Remaining)
}

func (badge *Badge) getAppid() {
	if node := badge.Node.Find("a", "class", "badge_row_overlay"); node.Error == nil {
		value := node.Attrs()["href"]
		badge.Appid = strings.Split(value, "/")[6]
	}
}

func (badge *Badge) getTitle() {
	if node := badge.Node.Find("div", "class", "badge_title"); node.Error == nil {
		badge.Title = strings.TrimSpace(node.Text())
	}
}

func (badge *Badge) getPlaytime() {
	node := badge.Node.Find("div", "class", "badge_title_stats_playtime")
	value := parseValue(node, " hrs ", 0)
	if playtime, err := strconv.ParseFloat(value, 0); err == nil {
		badge.Playtime = playtime
	}
}

func (badge *Badge) getReceived() {
	node := badge.Node.FindAll("div", "class", "card_drop_info_header")[1]
	value := parseValue(node, " received: ", 1)
	if received, err := strconv.ParseInt(value, 10, 0); err == nil {
		badge.Received = received
	}
}

func (badge *Badge) getRemaining() {
	node := badge.Node.Find("span", "class", "progress_info_bold")
	value := parseValue(node, " card drop", 0)
	if remaining, err := strconv.ParseInt(value, 10, 0); err == nil {
		badge.Remaining = remaining
	}
}

func parseValue(node soup.Root, separator string, index int) string {
	if node.Error != nil {
		return "0"
	}
	text := strings.TrimSpace(node.Text())
	if arr := strings.Split(text, separator); len(arr) > 1 {
		return arr[index]
	}
	return "0"
}

func getNextPage(node soup.Root) string {
	pageBtns := node.FindAll("a", "class", "pagebtn")
	for _, pageBtn := range pageBtns {
		if pageBtn.Text() == ">" {
			return pageBtn.Attrs()["href"]
		}
	}
	return ""
}
