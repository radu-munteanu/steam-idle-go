package main

import (
	"fmt"
)

func main() {
	client := NewClient()

	loginResponse := Login(&client, "")

	if loginResponse.RequiresTwofactor {
		var twoFactor string
		fmt.Print("Enter two-factor code: ")
		fmt.Scanf("%s", &twoFactor)

		loginResponse = Login(&client, twoFactor)
	}

	if loginResponse.LoginComplete {
		client.Steamid = loginResponse.TransferParameters.Steamid
		GetBadges(&client)
		fmt.Printf("Found %d games with remaining card drops.\n", len(client.Badges))
		fmt.Printf("Idling %d games, checking for progress every %d minutes.\n",
			client.ConcurrentGames, client.RefreshTimeout)
	} else {
		fmt.Printf("Login failed: %s\n", loginResponse.Message)
	}
}
