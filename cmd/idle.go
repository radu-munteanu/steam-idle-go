package main

// #cgo LDFLAGS: -ldl
// #include <dlfcn.h>
// int SteamAPI_Init(char* steam_api_file) {
//   typedef void func_signature(void);
//   void* steam_api = dlopen(steam_api_file, RTLD_LAZY);
//   func_signature* steam_api_init = (func_signature*) dlsym(steam_api, "SteamAPI_Init");
//   steam_api_init();
// }
import "C"
import (
	"os"
)

func main() {
	os.Setenv("SteamAppId", os.Args[2])
	if success, err := C.SteamAPI_Init(C.CString(os.Args[1])); success == 0 {
		panic(err)
	}
}
