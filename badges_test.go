package main

import (
	"testing"

	"github.com/anaskhan96/soup"
)

const htmlFragment = `
<div class="badge_row is_link">
    <a class="badge_row_overlay" href="https://steamcommunity.com/id/mock-user/gamecards/12345/"></a>
    <div class="badge_row_inner">
        <div class="badge_title_row">
            <div class="badge_title_stats">
                <div class="badge_title_stats_content">
                    <div class="badge_title_stats_playtime">
                        &nbsp; 2.6 hrs on record </div>
                    <div class="badge_title_stats_drops">
                        <span class="progress_info_bold">1 card drop remaining</span>
                        <div class="card_drop_info_header">Card drops earned: 4</div>
                        <div class="card_drop_info_body">
                            <div>Drops earned by purchasing: 4</div>
                        </div>
                        <div class="card_drop_info_header">Card drops received: 3 </div>
						<div class="card_drop_info_body">You can get 1 more trading card by playing Mock Title. </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="badge_title">
            Mock Title&nbsp;<span class="badge_view_details">View details</span>
        </div>
    </div>
</div>
`

func TestNewBadge(t *testing.T) {
	root := soup.HTMLParse(htmlFragment)
	badgeRow := root.Find("div", "class", "badge_row")

	badge, _ := NewBadge(badgeRow)

	got := badge.Appid
	if got != "12345" {
		t.Errorf("Appid = %s; want 12345", got)
	}

	got = badge.Title
	if got != "Mock Title" {
		t.Errorf("Title = %s; want Mock Title", got)
	}
}
