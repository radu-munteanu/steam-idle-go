package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"strings"
	"syscall"
	"time"

	"golang.org/x/crypto/ssh/terminal"
	"gopkg.in/yaml.v2"
)

// Client holds all information needed to track a session
type Client struct {
	Credentials struct {
		Username string `yaml:"username"`
		Password string `yaml:"password"`
	} `yaml:"credentials"`
	Steamid         string
	Badges          []Badge
	SteamAPIPath    string `yaml:"steam_api_path"`
	RefreshTimeout  int    `yaml:"refresh_timeout"`
	ConcurrentGames int    `yaml:"concurrent_games"`
	HTTPClient      http.Client
}

// NewClient collects credentials needeed to authenticate user
func NewClient() Client {
	var client Client
	client.getHTTPClient()

	configFile, err := ioutil.ReadFile("config.yml")

	if err != nil {
		// Set default values for RefreshTimeout and ConcurrentGames
		client.getRefreshTimeout()
		client.getConcurrentGames()
		fmt.Println("Config file 'config.yml' not found. Read credentials from stdin.")
		client.getUsername()
		client.getPassword()
		return client
	}

	err = yaml.Unmarshal(configFile, &client)

	if err != nil {
		panic("Unable to parse 'config.yml'")
	}

	if client.RefreshTimeout == 0 {
		client.getRefreshTimeout()
	}
	if client.ConcurrentGames == 0 {
		client.getConcurrentGames()
	}
	if client.Credentials.Username == "" {
		client.getUsername()
	}
	if client.Credentials.Password == "" {
		client.getPassword()
	}

	return client
}

func (client *Client) getHTTPClient() {
	cookieJar, err := cookiejar.New(nil)
	if err != nil {
		panic("Unable to create cookie jar")
	}
	client.HTTPClient = http.Client{Jar: cookieJar, Timeout: time.Duration(120 * time.Second)}
}

func (client *Client) getRefreshTimeout() {
	client.RefreshTimeout = 30
}

func (client *Client) getConcurrentGames() {
	client.ConcurrentGames = 1
}

func (client *Client) getUsername() {
	fmt.Print("Enter username: ")
	fmt.Scanf("%s", &client.Credentials.Username)
}

func (client *Client) getPassword() {
	fmt.Print("Enter password: ")
	bytePassword, _ := terminal.ReadPassword(int(syscall.Stdin))
	client.Credentials.Password = strings.TrimSpace(string(bytePassword))
	fmt.Println()
}
