# Steam Idle Go

A tiny application to idle in your Steam games.

### Development

While developing, the application should be run with
`go build && go build -o idle ./cmd && ./steam-idle-go`,
since there are two applications with a `main` func. This makes it possible to switch
cleanly between games to idle, since the process running a game needs to stop before
a new game can be started.

A naive example of idling two games sequentially could look something like this:

    cmd := exec.Command("./idle", "./libsteam_api64.so", "772430")
    cmd.Start()
    time.Sleep(30 * time.Minute)

    cmd = exec.Command("./idle", "./libsteam_api64.so", "365880")
    cmd.Start()
    time.Sleep(30 * time.Minute)

Though that would require manual configuration, and is error prone.
