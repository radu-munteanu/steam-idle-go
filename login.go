package main

import (
	"crypto/rand"
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"math/big"
	"net/url"
	"strconv"
	"time"
)

// Login performs login with user credentials
func Login(client *Client, twoFactor string) *LoginResponse {
	rsaKeyResponse := getRsaKeyResponse(client)
	password, _ := encryptPassword(client.Credentials.Password, rsaKeyResponse.PublickeyMod, rsaKeyResponse.PublickeyExp)
	return getLoginResponse(client, password, twoFactor, rsaKeyResponse.Timestamp)
}

// LoginResponse represents the login response from the server
type LoginResponse struct {
	Success            bool   `json:"success"`
	RequiresTwofactor  bool   `json:"requires_twofactor"`
	LoginComplete      bool   `json:"login_complete"`
	Message            string `json:"message,omitempty"`
	TransferParameters struct {
		Steamid string `json:"steamid"`
	} `json:"transfer_parameters"`
}

func getLoginResponse(client *Client, password, twoFactor, timestamp string) *LoginResponse {
	response, err := client.HTTPClient.PostForm("https://steamcommunity.com/login/dologin/", url.Values{
		"username":      {client.Credentials.Username},
		"password":      {password},
		"rsatimestamp":  {timestamp},
		"twofactorcode": {twoFactor},
	})

	if err != nil {
		return nil
	}

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return nil
	}

	var loginResponse LoginResponse
	err = json.Unmarshal(body, &loginResponse)
	if err != nil {
		return nil
	}
	return &loginResponse
}

// RsaKeyResponse represents the rsa "preflight" response from the server
type RsaKeyResponse struct {
	Success      bool   `json:"success"`
	PublickeyMod string `json:"publickey_mod"`
	PublickeyExp string `json:"publickey_exp"`
	Timestamp    string `json:"timestamp"`
	TokenGid     string `json:"token_gid"`
}

func getRsaKeyResponse(client *Client) *RsaKeyResponse {
	response, err := client.HTTPClient.PostForm("https://steamcommunity.com/login/getrsakey/", url.Values{
		"username":   {client.Credentials.Username},
		"donotcache": {strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)},
	})

	if err != nil {
		return nil
	}

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return nil
	}

	var rsaKeyResponse RsaKeyResponse
	err = json.Unmarshal(body, &rsaKeyResponse)
	if err != nil {
		return nil
	}
	return &rsaKeyResponse
}

func encryptPassword(plaintextPassword, modulus, exponent string) (string, error) {
	var publicKey rsa.PublicKey

	privateKey, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		return "", err
	}

	privateKey.Precompute()
	if err := privateKey.Validate(); err != nil {
		return "", err
	}

	if mod, ok := new(big.Int).SetString(modulus, 16); ok {
		publicKey.N = mod
	} else {
		return "", err
	}

	if exp, ok := new(big.Int).SetString(exponent, 16); ok {
		publicKey.E = int(exp.Int64())
	} else {
		return "", err
	}

	encrypted, err := rsa.EncryptPKCS1v15(rand.Reader, &publicKey, []byte(plaintextPassword))
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(encrypted[0:len(encrypted)]), nil
}
